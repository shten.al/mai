# Seminar 1

## Начало работа с github
Выполненно

## Начало работы с vagrant
Установил, но как с ним работать непонятно

## Обновить ядро на 5e
```bash
uname -mrs
wget https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
gpg --quiet --with-fingerprint RPM-GPG-KEY-elrepo.org 
rpm --import RPM-GPG-KEY-elrepo.org

wget http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
yum install elrepo-release-7.0-2.el7.elrepo.noarch.rpm
yum list available --disablerepo='*' --enablerepo=elrepo-kernel
yum --disablerepo='*' --enablerepo=elrepo-kernel install kernel-lt
```
не получилось - последние две строчи Ubuntu не понимает

## Команды для работы  с модулями ядра и не только
```
lsmod
modprobe
rmmod
modinfo module_name
systool -v -m module_name
# директория с конфигами модулей
/etc/modules-load.d/

sysctl -a
cat /etc/sysctl.conf
```
Непонятно

## Загрузка и сборка собственнного ядра
Попробую в другой жизни, а то вообще придеться все переустанавливать

## Попасть в систему без пароля
Выполненно

## Переименуем том LVM и попробуем загрузиться
что это?

```bash
# vgrename VolGroup00 MAIRoot
```

## Починим сломанную систему
Не понятно от каких поломок чинит
```bash
sed -i 's/VolGroup00/MAIRoot/g' /etc/fstab
sed -i 's/VolGroup00/MAIRoot/g' /boot/grub2/grub.cfg 
sed -i 's/VolGroup00/MAIRoot/g' /etc/default/grub
dracut -f -v
```
